## How to install project:
- Clone the project https://gitlab.com/test-task7720724/testTask
- Open as Maven project

## Required tools:
- Maven
- JDK 1.8+

## How to run tests
- Click Run on src/test/java/starter/TestRunner.java
- To run all tests by command "mvn clean verify"
- To run specific tests "mvn clean verify -Dcucumber.filter.tags=@Test-2 and not @Test-3"

## How to create new tests
- Create new branch with template feature/JIRA_ID_SHORT_DESCRIPTION 
- Search in TAF to reuse existed code|steps if applicable
- Extend existed feature file or create new feature file in package named with related functionality
- Extend existed Step Definitions or create new Step Definition in package named with related functionality
- Implement new tests
- Verify that its works locally
- Pull changes from remote
- Commit and push your changes
- Verify that CI/CD pipeline passed
- Create MR, select Assignee and Reviewers
- Inform reviewers about MR
- Resolve all discussions if present and verify that MR has at least 2 likes


## What was refactored
- Removed unneeded files, folders related to gradle, maven.yml, github, folders
- Updated dependency versions
- cleaned up serenity.conf
- Added tags for tests
- Moved CarsAPI steps to src/main/java package
- Updated tags variable to be able to run specific tests
- Implemented POJO
- create .gitlab-ci.yml