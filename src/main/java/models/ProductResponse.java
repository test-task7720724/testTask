package models;

import lombok.Data;

@Data
public class ProductResponse {
    public String provider;
    public String title;
    public String url;
    public String brand;
    public Double price;
    public String unit;
    public Boolean isPromo;
    public String promoDetails;
    public String image;

}