package models;

import lombok.Data;

@Data
public class ErrorResponseBody {
    public Detail detail;
}
@Data
class Detail {

    public Boolean error;
    public String message;
    public String requestedItem;
    public String servedBy;

}