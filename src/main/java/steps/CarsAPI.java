package steps;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public class CarsAPI {

    public RequestSpecification getRequestSpecification() {
        return SerenityRest.given().baseUri(System.getProperty("webdriver.base.url")).log().all();
    }
}
