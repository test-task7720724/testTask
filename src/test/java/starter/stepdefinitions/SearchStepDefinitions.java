package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.ProductResponse;
import net.thucydides.core.annotations.Steps;
import steps.CarsAPI;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @When("user calls endpoint {string}")
    public void userCallsEndpoint(String product) {
        userSendsRequest("GET", product);
    }

    @When("user sends {string} request for {string}")
    public void userSendsRequest(String method, String path) {
        carsAPI.getRequestSpecification().request(method, path);
    }

    @Then("user sees the results displayed for {string}")
    public void userSeesTheResultsDisplayedForProduct(String product) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.extract().as(ProductResponse[].class));
    }

    @Then("user does not see the results")
    public void user_Does_Not_See_The_Results() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

    @Then("user sees authenticated error")
    public void user_sees_authenticated_error() {
        restAssuredThat(response -> response.statusCode(401));
        restAssuredThat(response -> response.body("detail", equalTo("Not authenticated")));
    }

    @Then("user sees error Method not allowed")
    public void user_sees_method_not_allowed() {
        restAssuredThat(response -> response.statusCode(405));
        restAssuredThat(response -> response.body("detail", equalTo("Method Not Allowed")));
    }
}
