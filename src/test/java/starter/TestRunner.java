package starter;

import io.cucumber.junit.CucumberOptions;
import io.restassured.filter.log.LogDetail;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.serenitybdd.rest.SerenityRest;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        tags = "not @ignored"
)
public class TestRunner {

    @BeforeClass
    public static void beforeAll() {
        SerenityRest.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL);
    }
}
