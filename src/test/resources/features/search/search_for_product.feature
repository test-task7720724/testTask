Feature: Search for the product

  @Test-1
  Scenario Outline: The user searches for the <product>
    When user calls endpoint "<product>"
    Then user sees the results displayed for "<product>"
    Examples:
      | product |
      | orange  |
      | apple   |
      | cola    |
      | pasta   |

  @Test-2
  Scenario Outline: The user searches for the incorrect product
    When user calls endpoint "<product>"
    Then user does not see the results
    Examples:
      | product |
      | car     |

  @Test-3
  Scenario: The user searches with empty request
    When user calls endpoint ""
    Then user sees authenticated error

  @Test-4
  Scenario Outline: The user search for product using incorrect method <method>
    When user sends "<method>" request for "orange"
    Then user sees error Method not allowed
    Examples:
      | method |
      | POST   |
      | PUT    |
      | DELETE |